package problem2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/* 
 * merged_file_1 merged two sorted int file
 * merged_file_2 merged two sorted string file
 */

public class MergeSortedFiles {
	public static void main(String[] args) {
		String file1 = "resources/sorted_file_1";
		String file2 = "resources/sorted_file_2";
		String target = "resources/merged_file_1";
		merge(file1, file2, target);
		
		String file3 = "resources/sorted_string_1";
		String file4 = "resources/sorted_string_2";
		String target2 = "resources/merged_file_2";
		merge(file3, file4, target2);
	}
	
	public static void merge(String file1, String file2, String targetFile) {
		try (BufferedReader br1 = new BufferedReader(new FileReader(file1));
				BufferedReader br2 = new BufferedReader(new FileReader(file2));
				BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile));) {
			String cur_1 = br1.readLine();
			String cur_2 = br2.readLine();
			while (cur_1 != null || cur_2 != null) {
				if (cur_2 == null || (cur_1 != null && cur_1.compareTo(cur_2) <= 0)) {
					writer.write(cur_1);
					cur_1 = br1.readLine();
				} else {
					writer.write(cur_2);
					cur_2 = br2.readLine();
				}
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("merge complete");
	}
}

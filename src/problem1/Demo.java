package problem1;

public class Demo {

	public static void main(String[] args) {
		String[] fileNames = {"resources/ticks_price_1", "resources/ticks_price_2"};
		ClosingPricePrinter pricePrinter = new ClosingPricePrinter();
		pricePrinter.load(fileNames);
		System.out.println("-------print all--------");
		pricePrinter.printAll();
		System.out.println("-------print price by name--------");
		pricePrinter.printPrice("GoldmanS");
		pricePrinter.printPrice("Facebook");
	}
}

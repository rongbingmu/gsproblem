package problem1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/* 
 * ClosingPricePrinter is designed to load files and store the closing price
 * of ticks in a map. It only writes to the map when the current line is a new
 * tick name. It supports multiple files loading.
 * Use printAll() method or printPrice(String tickName) to print closing price.
 * 
 * I assumed one file can have same tick name in different location
 * I assumed file names are ordered by time
 */

public class ClosingPricePrinter {
	private final Map<String, Double> tickPrice;
	private String prevTick;
	private Double prevPrice;
	
	public ClosingPricePrinter(){
		tickPrice = new HashMap<>();
		prevTick = "";
		prevPrice = 0.0;
	}
	
	public void load(String[] fileNames) {
		for(String fileName: fileNames) {
			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
				stream.forEach(s -> {
					if(isTickName(s)) {
						tickPrice.put(prevTick, prevPrice);
						prevTick = s;
					} else {
						prevPrice = Double.parseDouble(s);
					}
				});
				tickPrice.put(prevTick, prevPrice);
				tickPrice.remove("");
				System.out.println("File " + fileName + " is loaded");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("-------all files loaded-------");
	}
	
	private static boolean isTickName(String s) {
		return s.toCharArray().length == 8 && s.split(".").length == 0;
	}
	
	public void printAll() {
		tickPrice.entrySet().forEach(this::printPrice);
	}
	
	private void printPrice(Map.Entry<String, Double> entry) {
		printPrice(entry.getKey());
	}
	
	public void printPrice(String tickName) {
		System.out.println("The closing price for " + tickName + 
				" is " + tickPrice.get(tickName));
	}

}